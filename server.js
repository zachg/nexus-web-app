const express = require('express')
const CONFIG = require('./config')

// Create parent application object
const nexus = express()

nexus.use(express.static('static'))



nexus.listen(CONFIG.SERVER.LISTEN_PORT, (err) => {
    if (err) {
        console.error('Error', err)
    } else {
        console.log('Server listening on port', CONFIG.SERVER.LISTEN_PORT)
    }
})