# This dockerfile tells Docker how to build the container.
FROM node:latest

#WORKDIR /Users/zgillis/IdeaProjects/NexusTempSite

COPY . .

RUN echo "SETTING UP DOCKERIZATION OF NEXUSSITE, CHEERS"
RUN npm install

EXPOSE 3000
CMD ["node", "server.js"]
